<?php

namespace Drupal\redis_cache_whitelist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure Redis Whitelist settings for this site.
 */
class RedisCacheWhitelistAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'redis_cache_whitelist_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['redis_cache_whitelist.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('redis_cache_whitelist.settings');

    $form['whitelist_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Contents of whitelist'),
      '#default_value' => $config->get('content'),
      '#cols' => 60,
      '#rows' => 20,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('redis_cache_whitelist.settings');

    $list = $form_state->getValue('whitelist_content');

    $config
      ->set('content', $list)
      ->save();

    Cache::invalidateTags(['redis_cache_whitelist']);
    parent::submitForm($form, $form_state);
  }

}

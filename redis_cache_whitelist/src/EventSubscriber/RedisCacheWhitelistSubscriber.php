<?php

namespace Drupal\redis_cache_whitelist\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Site\Settings;

/**
 * Class RedisCacheWhitelistSubscriber.
 *
 * @package Drupal\redis_cache_whitelist\EventSubscriber
 */
class RedisCacheWhitelistSubscriber implements EventSubscriberInterface {

  /**
   * Function checks whether to use redis cache or not.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event.
   */
  public function checkForRedisCaching(GetResponseEvent $event) {

    $redis_settings = Settings::get('redis.connection');
    $redis_enabled = (isset($redis_settings['interface']) && $redis_settings['interface'] == 'PhpRedis');

    if ($redis_enabled) {

      // Get current URI.
      $current_path = \Drupal::service('path.current')->getPath();

      // Get the whitelist as an array.
      $redis_whitelist = \Drupal::config('redis_cache_whitelist.settings')->get('content');

      // Do comparison.
      $use_cache = \Drupal::service('path.matcher')->matchPath($current_path, $redis_whitelist);

      // Disable page cache temporarily.
      if (!$use_cache) {
        \Drupal::service('page_cache_kill_switch')->trigger();
        return;
      }

    } // End Redis is enabled.

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedisCaching'];
    return $events;
  }

}
